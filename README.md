# Plano Theme (Fork)

This is a fork of the great [Plano Theme](https://github.com/lassekongo83/plano-theme). I just made some minor changes for Openbox:

* Display grey border around the active window
* Show window handle (for re-sizing)
* Changed maximize icon
* Window buttons: grey background on hover (close button: white X on red background, similar to Windows)